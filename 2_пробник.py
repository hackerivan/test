
#1

def f(x,y,z):
    return not x and not y or not z and not x or z and x 


for x in [False,True]:
    for y in [False,True]:
        for z in [False,True]:
            print(x,y,z, f(x,y,z))
